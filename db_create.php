<?php
include (__DIR__.'/src/System/config.php');
$conn = new mysqli(HOST, USER, PASS, DB, PORT);
if ($conn->connect_error) {
    die("Ошибка подключения: " . $conn->connect_error);
}
// Установка данных в таблицу
$sql = "
CREATE TABLE IF NOT EXISTS `music` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artist` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isLike` int(11) NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int(11) NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_vk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_inst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `can_see_audio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `audios` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crated` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO `users` (`id`, `login_vk`, `login_inst`, `phone`, `can_see_audio`, `first_name`, `last_name`, `city`, `audios`, `photo`, `crated`, `updated`) VALUES
(1, '163881307', NULL, '', '1', 'Денис', 'Шамкаев', 'Октябрьский', NULL, '/data/5f81bcfcd8d01.jpeg', '2020-10-10 13:54:04', '2020-10-10 13:54:04')";

if ($conn->query($sql) === TRUE) {
    echo "Успешно создана новая запись";
} else {
    echo "Ошибка: " . $conn->error;
}
