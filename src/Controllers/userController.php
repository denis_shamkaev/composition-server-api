<?php


namespace Controllers;

use Model\vkModel;
use Vodka2\VKAudioToken\CommonParams;
use Vodka2\VKAudioToken\SupportedClients;
use Vodka2\VKAudioToken\TokenException;
use Vodka2\VKAudioToken\TwoFAHelper;

/**
 * Class userController
 * @package Controllers
 */
class userController
{
    public function setUserAction()
    {
        if (!$_POST){
            die;
        }
        $email    = '';
        $password = '';
        $code     = 'GET_CODE';
        if (isset($_POST['email'])){
            $email = $_POST['email'];
        }
        if (isset($_POST['password'])){
            $password = $_POST['password'];
        }
        if (isset($_POST['code'])){
            $code = $_POST['code'];
        }
        $model = new vkModel();
        try {
            $data  = $model->getVkOfficialToken($email, $password, $code);
            $data['status'] = "ok";
            exit(json_encode($data));

        } catch (TokenException $e){
            $extra = (array)$e->extra;
            if ($extra["error"] == 'need_validation' && $code != 'GET_CODE') {
                if ($e->getCode() == TokenException::TWOFA_REQ && isset($e->extra->validation_sid)) {
                    $user_agent = SupportedClients::VkOfficial()->getUserAgent();
                    $params     = new CommonParams($user_agent);
                    (new TwoFAHelper($params))->validatePhone($e->extra->validation_sid);
                    exit(json_encode(['status' => 'ok']));
                }
            }
            $extra['status'] = 'error';
            exit(json_encode($extra));
//  "error" => "invalid_client"
//  "error_description" => "Неправильный логин или пароль"
//  "error_type" => "username_or_password_is_incorrect"

//"error" => "need_captcha"
//"captcha_sid" => "427416572544"
//"captcha_img" => "https://api.vk.com/captcha.php?sid=427416572544"
//"status" => "error"
        }

    }

    public function updateUserAction($id){

    }


}