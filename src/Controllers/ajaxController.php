<?php
namespace Controllers;
use Entity\Radio;
use Model\cacheModel;
use Model\MultimediaModel;
use Model\musicModel;
use Model\radioModel;
use Model\userModel;
use Model\vkModel;
use Repository\UserRepository;

class ajaxController
{

    public function __construct()
    {
    }

    function handlerErrorAction(){
        if (!file_exists('errors.txt')){
            file_put_contents('errors.txt', '');
        }
        $data = $_POST;
        $text = json_encode($data);
        $fp   = fopen('errors.txt', 'a');
        fwrite($fp, $text . PHP_EOL);
        fclose($fp);
        exit('ok');
    }

    function checkUpdateAction(float $version){
        $this_ver = VERSION;
        if ($this_ver > $version){
           exit(file_get_contents('update.html'));
        }
        exit();
    }

    /**
     * Проверено!
     * Возврат методов и параметров
     */
    function indexAction(){
        $class_methods = get_class_methods(new $this);
        $result = [];
        foreach ($class_methods as $key => $class_method) {
            if ($class_method == 'indexAction')continue;
            $reflection = new \ReflectionMethod($this, $class_method);
            foreach ($reflection->getParameters() as $param){
                $result[$class_method][] = $param->name;
            }
        }
        dump($result);
        dump($class_methods);
    }

    function autoCompleteAction($word){
        $model = new musicModel();
        $word  = $model->autoComplete($word);
        exit($word);
    }

    /**
     * Проверено!
     * Функция возврата популярной музыки
     * @param String $page
     */
    function getPopularAction($page = null)
    {
        $page  = $page ? : 1 ;
        $url   = "https://muzofond.fm/{$page}";
        $model = new musicModel();
        $songs = $model->getMusic($url);
        if ($_SERVER['HTTP_USER_AGENT'] == 'composition bot'){
            return json_decode($songs);
        }
        exit($songs);
    }

    /**
     * Проверено!
     * Функция поиска
     * @param string $name
     * @param string|null $page
     */
    function getSearchAction(string $name, string $page = null)
    {
        $name   = urldecode($name);
        $page   = $page ? $page : 1;
        $model  = new musicModel();
        $url    = "https://muzofond.fm/search/$name/$page";
        $songs  = $model->getMusic($url,null,null,true);
        exit($songs);
    }
    /**
     * Проверено!
     * Функция поиска
     * @param string $name
     * @param string|null $page
     */
    function startSearchSearchAction(string $name, string $page = null)
    {
        $name   = urldecode($name);
        $page   = $page ? $page : 1;
        $model  = new musicModel();
        $url    = "https://muzofond.fm/search/$name/$page";
        $GLOBALS['search']  = true;
        $songs  = $model->getMusic($url);
        exit($songs);
    }

    /**
     * Проверено!
     * Функция возврата музыки по жанрам
     * @param $genre
     * @param $page
     * @return mixed
     */
    function getGenreAction($genre, $page = null)
    {
        $page = $page ? : 1;
        $genre = preg_replace('/@/','/',$genre);
        if ($genre == 'undefined'){
            return $this->getPopularAction($page);
        }
        $model  = new musicModel();
        $songs  = $model->getMusic("https://muzofond.fm/$genre/$page");
        if ($_SERVER['HTTP_USER_AGENT'] == 'composition bot'){
            return (json_decode($songs));
        }
        exit($songs);
    }

    /**
     * Функция возврата жанров
     */
    function getGenresAction(){
        $model  = new musicModel();
        $genres = $model->getGenres();
        exit(json_encode($genres));
    }

    function getRadiosAction(){
        $model = new radioModel();
        $radio = $model->getRadios();
        exit(json_encode($radio));
    }

    function AddRadioAction(){
        $radio = [];
        $new_radio = [];
        if (($handle = fopen("./radio.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $radio[] = $data;
            }
            fclose($handle);
        }

        foreach ($radio as $key => $item){
            $n_radio = new Radio();
            $url = $item[2];
            $url = parse_url($url)['path'];
            $url = explode('/', $url);
            $name = "./radio/".array_pop($url);
            $file = file_get_contents($item[2]);
            file_put_contents($name, $file);

            $n_radio->setName($item[0]);
            $n_radio->setFm($item[1]);
            $n_radio->setImg($name);
            $n_radio->setUrl($item[3]);
            $n_radio->setOrigin($item[4]);
            $new_radio[] =$n_radio;
        }
        $model = new radioModel();
        foreach ($new_radio as $radio){
            $model->addRadio($radio);
        }
        $radio = $model->getRadios();
        echo json_encode($radio, true);
        dump($radio);
    }


    /**
     * Возвращает картинку интернета или из базы
     * @param $artist
     * @param $song
     */
    function getImageAction($artist,$song){
        if (empty($artist) || empty($song)){
            die;
        }
        $artist     = urldecode($artist);
        $song       = urldecode($song);
        $model      = new  MultimediaModel();
        $song_img   = $model->getImage($song, $artist);
        $artist_img = $model->getArtistImage($artist);
        $result = [
            'artist' => $artist_img,
            'song'   => $song_img,
        ];
        echo(json_encode($result));
        die;
    }

    function getUserAction(){
        if (!$_SESSION['id']) {
            return false;
        }
        $repo = new UserRepository();
        $user = $repo->getUserById($_SESSION['id']);
        exit(json_encode($user));
    }

    /**
     * @param $vk_id
     * @param $token
     * @param $user_agent
     * @return false
     */
    function getVkMusicAction($vk_id, $token, $user_agent = null){

        $vkModel = new vkModel();
        //Mozilla/5.0 (iPad; CPU OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1
        //Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; GT-P5210 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30
        $user_agent = "VKAndroidApp/5.52-4543 (Android 5.1.1; SDK 22; x86_64; Composition; ru; 320x240)";
        $user_agent = "com.vk.vkclient/746 (unknown, iOS 13.3, iPhone8,1, Scale/2.0)";
        $data = $vkModel->getAudiosUser($vk_id, $token);
        exit(json_encode($data));
    }
}