<?php
namespace Controllers;
use Model\userModel;
use Model\vkModel;
use System\App;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;
use Vodka2\VKAudioToken\CommonParams;
use Vodka2\VKAudioToken\SupportedClients;
use Vodka2\VKAudioToken\TokenException;
use Vodka2\VKAudioToken\TwoFAHelper;

/**
 * Class vkontakteController
 * @package Controllers
 */
class vkontakteController
{

    public function getUrlAction(){
        $oauth        = new VKOAuth();
        $client_id    = 6383380;
        $redirect_uri = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['SERVER_NAME']}/auth/vkontakte";
        $display      = VKOAuthDisplay::PAGE;
        $scope        = [VKOAuthUserScope::WALL, VKOAuthUserScope::GROUPS];
        $state        = 'secret_state_code';
        return $oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $client_id, $redirect_uri, $display, $scope, $state);
    }

    /**
     * @param null $url_base
     * @param null $post
     * @param null $extra
     * @param null $too_fa
     */
    public function getWindowAction($url_base = null ,$post = null, $extra = null, $too_fa = null){
//        $model = new userModel();
//        $user = $model->getUserById('13', true);
//        App::render('token.html.twig', ['data' => json_encode($user)]);
        $url     = $url_base ? $url_base : $this->getUrlAction();
        $url     = urldecode($url);
        $model   = new vkModel();
        $content = $model->file_get_content($url, $post);
        if ($extra) {
            $captcha_img = $extra->captcha_img;
            $captcha_sid = $extra->captcha_sid;
            $capcha      = file_get_contents('./vk/script_capcha.html');
            $capcha      = str_replace('%captcha_sid%', $captcha_sid, $capcha);
            $capcha      = str_replace('%src%', $captcha_img, $capcha);
            $content     = str_replace('name="pass" />', $capcha, $content);
            $content     = str_replace('name="pass" />', $capcha, $content);
        }

        $content = str_replace('location.href = "https://oauth.vk.com/blank.html";', '', $content);
        $content = str_replace('<head>', '<meta charset="utf-8">', $content);
        $content = str_replace("innerHTML",'', $content);
        $content = str_replace('location.reload()','', $content);

        if ($too_fa){
            $fa      = file_get_contents('./vk/2_fa_html.html');
            $fa      = str_replace('%email%', $_POST['email'], $fa);
            $fa      = str_replace('%pass%', $_POST['pass'], $fa);
            $fa      = str_replace('%action%', "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['SERVER_NAME']}/vkontakte/setUser", $fa);
            $script  = str_replace('%html%',$fa, $this->script_2_fa());
            $content = str_replace('</body>', $script.'</body>', $content);
        } else {
            $content = str_replace('</body>', $this->script().'</body>', $content);
        }
        file_put_contents('test.html', $content);
        echo $content;
        die;
    }

    public function script(){
        return file_get_contents('./vk/script.html');
    }

    public function script_2_fa(){
        return file_get_contents('./vk/2_fa.html');
    }

    /* Example of getting mp3 from m3u8 url */
    function getMp3FromM3u8() {
        dump($_GET['url']);
        $url = $_GET['url'];
        // Not a m3u8 url
        if (!strpos($url, "index.m3u8?")) {
            dump($url);
        }
        if (strpos($url, "/audios/")) {
            dump(preg_replace('~^(.+?)/[^/]+?/audios/([^/]+)/.+$~', '\\1/audios/\\2.mp3', $url));
        } else {
            dump(preg_replace('~^(.+?)/(p[0-9]+)/[^/]+?/([^/]+)/.+$~', '\\1/\\2/\\3.mp3', $url));
        }
    }

    /**
     * @throws TokenException
     */
    function setUserAction(){
        $email    = @$_POST['email'];//
        $password = @$_POST['pass'];//
        $code     = 'GET_CODE';
        if (isset($_POST['code'])){
            $code = $_POST['code'];
        }
        $model = new vkModel();
        try {
            $data = $model->getVkOfficialToken($email, $password, $code);
            App::render('token.html.twig', ['data' => json_encode($data)]);
        } catch (TokenException $exception){
            $e = $exception;
            $exception = (array) $exception;
            if (isset($exception['extra'])){
                $error = (array)$exception['extra'];
                if (isset($error['error_description'])){
                    if ($error['error_description'] == 'Неправильный логин или пароль') {
                        return $this->getWindowAction(@$_POST['origin_v'], $_POST);
                    }
                }
            }
            if (isset($exception['extra'])){
                if ($exception['extra']->error == 'need_captcha') {
                    return $this->getWindowAction(@$_POST['origin_v'], $_POST,$exception['extra']);
                }
                if ($exception['extra']->error == 'need_validation') {
                    if ($e->getCode() == TokenException::TWOFA_REQ && isset($e->extra->validation_sid)) {
                        $user_agent = SupportedClients::Kate()->getUserAgent();
                        $params     = new CommonParams($user_agent);
                        (new TwoFAHelper($params))->validatePhone($e->extra->validation_sid);
                        return $this->getWindowAction(null, $_POST,null,$exception['extra']);
                    } else {
                        return $this->getWindowAction(null, $_POST,null,$exception['extra']);
                    }
                }
            }
        }

//        $user_id = $model->getUser($token,$user_agent);
//        $model->getAudiosUser($token,$user_agent,$user_id);
//        $model->getSearch($token,$user_agent,'чай в двоем');

    }

}