<?php

namespace Controllers;

use Model\musicModel;
use System\App;
use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class homeController {



    public function indexAction() {
        App::render('index.html.twig', [
        ], null);
    }

    public function pullAction() {
      $dir = __DIR__.'/../../pull.sh';
        $cmd = exec($dir);
      dump($cmd,$dir);
    }

    public function pushAction() {
        $dir = __DIR__.'/../../';
        $t = 'cd '.$dir.'&git commit -a&git push';
        $cmd = exec($t);
        dump($cmd,$dir,$t);
    }

    public function imageAction(){
        $song_name = current($_GET);
        $model  = new musicModel();
        $img =  $model->getImage($song_name) ? : 'https://ruv.hotmo.org/static/images/no-cover-300.jpg';
        echo $img;
        exit();
    }

    public function playerAction() {
        App::render('player.html.twig', [
        ]);
    }


}
