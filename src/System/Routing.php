<?php

namespace System;


class Routing
{
    function getRute()
    {
        $file       = file_get_contents(__DIR__ . '/routing.yaml');
        $routes     = yaml_parse($file, -1);
        $routes     = current($routes);
        $controller = false;
        foreach ($routes as $route){
            $path_t = ($route['path']);
            $path   = ($_SERVER['REQUEST_URI']);
            $path_s = preg_replace('/{.*.}/','',$path_t);
            if(strpos($path, $path_s) === FALSE) {
                continue;
            }
            $explode    = explode("::", $route['controller']);
            $controller = "Controllers\\{$explode[0]}";;
            if (!class_exists($controller)){
                throw new \ErrorException(('Controller '.$controller.' does not exist'));
            }
            $action     = $explode[1];
            $controller = new $controller;
            if (!method_exists($controller,$action)) {
                throw new \ErrorException(('Controller exist, '.$action.' does not exist'));
            }

            $params = preg_match('/{.*.}/',$path_t);
            if ($params) {
                $tmp = preg_replace('/{.*.}/', '',$path_t);
                $path = str_replace($tmp, '',$path);
                $tmp_params = explode('/',$path);
                return call_user_func_array(
                    [$controller, $action], # что вызывать
                    $tmp_params # аргументы
                );
            } else {
                return $controller->$action();
            }
        }
        if($controller === false){
            header('HTTP/1.0 403 Forbidden');
            die('Access denied');
        }
    }


}