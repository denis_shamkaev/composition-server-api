<?php
namespace System;
use Model\cacheModel;
use PDO;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
class App{
    /**
     * Start Application
     * @author DenisShamkaev
     * @throws \ErrorException
     */
    public static function run(){

        if (isset(getallheaders()['Authorization'])){
            $GLOBALS['serial'] = getallheaders()['Authorization'];
        }
        $routing = new Routing();
        $routing->getRute();
    }

    public static function authorization(){
        return $GLOBALS['serial'];
    }

    /**
     * @param $name
     * @param null $arr
     * @return void
     */
    public static function render($name, $arr = null){

        $loader = new FilesystemLoader(__DIR__.'/../Views');
        $twig   =  new Environment($loader, [
//    'cache' => '/path/to/compilation_cache',
        ]);
        try {
            echo $twig->render($name,$arr ? $arr : []);
        } catch (LoaderError $e) {
            dump($e->getMessage());
        } catch (RuntimeError $e) {
            dump($e->getMessage());
        } catch (SyntaxError $e) {
            dump($e->getMessage());
        }
    }
    public static $pdo;
    /**
     * @return PDO
     */
    public static function MySql(): PDO
    {
        $host = HOST;
        $db   = DB;
        $user = USER;
        $pass = PASS;
        $port = PORT;
        $charset = CHARSET;

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset;port=$port";
        if (!self::$pdo){
            self::$pdo = new PDO($dsn, $user, $pass);
        }
        try {
            return self::$pdo;
        } catch (\PDOException $e) {
            dump($e->getMessage());
            die;
        }

    }

}
