<?php
require __DIR__ . '/App.php';
require __DIR__ . '/SimpleImage.php';
$directories = ['Controllers', 'Entity', 'Model', 'Repository'];
foreach ($directories as $directory) {
    $files = scandir(__DIR__.'/../../src/'.$directory);
    unset($files[0]);
    unset($files[1]);
    foreach ($files as $file => $name) {
        autoload($directory,$name);
    }
}

if (!function_exists('getIp')) {
    function getIp()
    {
        try {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        } catch (\Exception $e){
        }

    }
}


if (!function_exists('getCache')) {
    function getCache($name){
        if (isset($GLOBALS['search'])){
            return false;
        }
        $name = base64_encode($name).".json";
        $name = preg_replace('/\//','',$name);
        $file = CACHE_DIR.$name;
        if(file_exists($file)) {
            return file_get_contents($file);
        }
        return false;
    }
}

if (!function_exists('convert')) {
    function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
}

if (!function_exists('setCache')) {
    function setCache($name, $data){
        $name = base64_encode($name).".json";
        $name = preg_replace('/\//','',$name);
        $file = CACHE_DIR.$name;
        file_put_contents($file,$data);
    }
}
$GLOBALS['cron'] = false;
if (isset($_SERVER['HTTP_USER_AGENT'])){
    if ($_SERVER['HTTP_USER_AGENT'] == 'composition bot'){
        $GLOBALS['cron'] = true;
    }
}






function autoload($directory,$class_name)
{
    include __DIR__ . "/../".$directory."/" . $class_name;
}
