<?php

namespace Vodka2\VKAudioToken;

class SupportedClients {
    public static function Kate(){
        return new VkClient(
            "KateMobileAndroid/56 lite-460 (Android 4.4.2; SDK 19; x86; Composition Android SDK built for x86; ru)",
            "lxhD8OD7dMsqtXIm5IUY",
            "2685278"
        );
    }
    public static function VkOfficial(){
        return new VkClient(
            "VKAndroidApp/5.52-4543 (Android 5.1.1; SDK 22; x86_64; Composition; ru; 320x240)",
            "hHbZxrka2uZ6jB1inYsH",
            "2274003"
        );
    }

    public static function VkOfficialIphone(){
        return new VkClient(
            "com.vk.vkclient/5.52-4543 (unknown, iPhone OS 7.1.2, iPhone, Scale/2.000000)",
            "VeWdmVclDCtn6ihuP1nt",
            "3140623"
        );
    }

    public static function VkOfficialIpad(){
        return new VkClient(
            "com.vk.vkclient/792 (unknown, iOS 13.3.1, iPhone10,4, Scale/2.0)",
            "mY6CDUswIVdJLCD3j15n",
            "3682744"
        );
    }
    public static function Boom(){
        return new VkClient(
            "VK_Music/4.2.1 (Android 5.1.1; SDK 22; x86_64; Composition Android SDK built for x86_64; ru; 320x240)",
            "",
            "4705861"
        );
    }
}