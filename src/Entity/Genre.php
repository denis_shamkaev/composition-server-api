<?php
namespace Entity;
/**
 * Class Genre
 * @package Entity
 */
class Genre
{
    /**
     * Genre constructor.
     * @param null $object
     */
    public function __construct($object = null)
    {
        if (!$object) {
            return $this;
        }
        foreach ($object as $name => $value) {
            $name = str_replace('_', ' ', $name);
            $name = ucwords($name);
            $name = str_replace(' ', '', $name);
            $name = 'set' . $name;
            if (method_exists($this, $name)) {
                $this->$name($value);
            }
        }
        return $this;
    }

    /**
     * @var Integer
     */
    private int $id;

    /**
     * @var mixed
     */
    private $name = '';

    /**
     * @var mixed
     */
    private $url = '';

    /**
     * @var mixed
     */
    private $img = '';

    /**
     * @var mixed
     */
    private $color = '';
    /**
     * @var mixed
     */
    private $other = '';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img): void
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param mixed $other
     */
    public function setOther($other): void
    {
        $this->other = $other;
    }

}