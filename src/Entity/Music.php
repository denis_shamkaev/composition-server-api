<?php
namespace Model;

/**
 * Class Music
 * @package Model
 */
class Music {

    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $song;
    /**
     * @var
     */
    private $artist;
    /**
     * @var
     */
    private $time;
    /**
     * @var
     */
    private $img;
    /**
     * @var
     */
    private $isLike;
    /**
     * @var
     */
    private $url;
    /**
     * @var
     */
    private $owner_id;
    /**
     * @var
     */
    private $date;
    /**
     * @var
     */
    private $vk_owner_id;
    /**
     * @var
     */
    private $vk_audio_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSong()
    {
        return $this->song;
    }

    /**
     * @param mixed $song
     */
    public function setSong($song)
    {
        $this->song = $song;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getIsLike()
    {
        return $this->isLike;
    }

    /**
     * @param mixed $isLike
     */
    public function setIsLike($isLike)
    {
        $this->isLike = $isLike;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->owner_id;
    }

    /**
     * @param mixed $owner_id
     */
    public function setOwnerId($owner_id)
    {
        $this->owner_id = $owner_id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getVkOwnerId()
    {
        return $this->vk_owner_id;
    }

    /**
     * @param mixed $vk_owner_id
     */
    public function setVkOwnerId($vk_owner_id)
    {
        $this->vk_owner_id = $vk_owner_id;
    }

    /**
     * @return mixed
     */
    public function getVkAudioId()
    {
        return $this->vk_audio_id;
    }

    /**
     * @param mixed $vk_audio_id
     */
    public function setVkAudioId($vk_audio_id)
    {
        $this->vk_audio_id = $vk_audio_id;
    }


    /**
     * Music constructor.
     * @param null $object
     */
    public function __construct($object = null)
    {
        if (!$object) {
            return $this;
        }
        foreach ($object as $name => $value) {
            $name = str_replace('_', ' ', $name);
            $name = ucwords($name);
            $name = str_replace(' ', '', $name);
            $name = 'set' . $name;
            if (method_exists(Music::class, $name)) {
                $this->$name($value);
            }
            return $this;
        }
    }
}