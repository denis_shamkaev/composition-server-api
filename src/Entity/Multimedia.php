<?php
namespace Entity;
/**
 * Class Multimedia
 * @package Entity
 */
class Multimedia{
    /**
     * Multimedia constructor.
     * @param null $object
     */
    public function __construct($object = null)
    {
        if (!$object) {
            return $this;
        }
        foreach ($object as $name => $value) {
            $name = str_replace('_', ' ', $name);
            $name = ucwords($name);
            $name = str_replace(' ', '', $name);
            $name = 'set' . $name;
            if (method_exists($this, $name)) {
                $this->$name($value);
            }
        }
        return $this;
    }

    public function toArray(){
        $vars  = get_object_vars ( $this );
        $array =[];
        foreach ( $vars as $key => $value ) {
            $array[ltrim( $key,'_' )] = $value;
        }
        return $array;
    }

    /**
     * @var Integer
     */
    private int $id;

    /**
     * @var mixed
     */
    private $artist = '';

    /**
     * @var mixed
     */
    private $song = '';

    /**
     * @var mixed
     */
    private $url_img = '';

    /**
     * @var mixed
     */
    private $url_mp3 = '';
    /**
     * @var mixed
     */
    private $color = '';

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getSong()
    {
        return $this->song;
    }

    /**
     * @param mixed $song
     */
    public function setSong($song)
    {
        $this->song = $song;
    }

    /**
     * @return mixed
     */
    public function getUrlImg()
    {
        return $this->url_img;
    }

    /**
     * @param mixed $url_img
     */
    public function setUrlImg($url_img)
    {
        $this->url_img = $url_img;
    }

    /**
     * @return mixed
     */
    public function getUrlMp3()
    {
        return $this->url_mp3;
    }

    /**
     * @param mixed $url_mp3
     */
    public function setUrlMp3($url_mp3)
    {
        $this->url_mp3 = $url_mp3;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }

}