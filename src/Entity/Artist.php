<?php
namespace Entity;
/**
 * Class Artist
 * @package Entity
 */
class Artist{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist): void
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getUrlImg()
    {
        return $this->url_img;
    }

    /**
     * @param mixed $url_img
     */
    public function setUrlImg($url_img): void
    {
        $this->url_img = $url_img;
    }
    /**
     * Multimedia constructor.
     * @param null $object
     */
    public function __construct($object = null)
    {
        if (!$object) {
            return $this;
        }
        foreach ($object as $name => $value) {
            $name = str_replace('_', ' ', $name);
            $name = ucwords($name);
            $name = str_replace(' ', '', $name);
            $name = 'set' . $name;
            if (method_exists($this, $name)) {
                $this->$name($value);
            }
        }
        return $this;
    }

    public function toArray(){
        $vars  = get_object_vars ( $this );
        $array =[];
        foreach ( $vars as $key => $value ) {
            $array[ltrim( $key,'_' )] = $value;
        }
        return $array;
    }

    /**
     * @var Integer
     */
    private int $id;

    /**
     * @var mixed
     */
    private $artist = '';

    /**
     * @var mixed
     */
    private $url_img = '';
    /**
     * @var mixed
     */
    private $color = '';

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }


}