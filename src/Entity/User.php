<?php

namespace Entity;
use Repository\UserRepository;

/**
 * Class User
 * @package Entity
 */
class User
{

    /**
     * User constructor.
     * @param $object
     */
    public function __construct($object = null)
    {
        if (!$object) {
            return $this;
        }
        $this->repo = new UserRepository();
        foreach ($object as $name => $value) {
            $name = str_replace('_', ' ', $name);
            $name = ucwords($name);
            $name = str_replace(' ', '', $name);
            $name = 'set' . $name;
            if (method_exists($this, $name)) {
                $this->$name($value);
            }
        }
        return $this;
    }

    /**
     * @var UserRepository
     */
    private $repo;

    private $id;

    private $login_vk;

    private $login_inst;

    private $phone;

    private $photo;

    private $audio;

    private $can_see_audio;

    private $first_name;

    private $last_name;

    private $city;

    private $created_at;

    private $updated_at;

    private $login;

    private $pass;

    private $ip;

    private $token;

    private $agent;

    private $serial;

    public function update($user){
        $this->repo->updateUser($user);
    }

    public function create($user){
        $this->repo->addUser($user);
    }

    /**
     * @return mixed
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * @param mixed $serial
     */
    public function setSerial($serial): void
    {
        $this->serial = $serial;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }


    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * @param mixed $audio
     */
    public function setAudio($audio)
    {
        $this->audio = $audio;
    }



    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getFirstName(){
        return $this->first_name;
    }

    public function setFirstName($fistName){
        $this->first_name = $fistName;
    }

    public function getLastName(){
        return $this->last_name;
    }

    public function setLastName($lastName){
        $this->last_name = $lastName;
    }

    public function getLoginVk(){
        return $this->login_vk;
    }

    public function setLoginVk($login){
        $this->login_vk = $login;
    }

    public function getLoginInst(){
        return $this->login_inst;
    }

    public function setLoginInst($login){
        $this->login_inst = $login;
    }

    public function getPhone(){
        return $this->phone;
    }

    public function setPhone($phone){
        $this->phone = $phone;
    }

    public function getCity(){
        return $this->city;
    }

    public function setCity($city){
        $this->city = $city;
    }

    public function getCreated(){
        return $this->created_at ? : $this->getUpdated();
    }

    public function getUpdated(){
        return $this->updated_at;
    }

    public function setUpdated($date){
        $this->updated_at = $date;
    }

    public function getPhoto(){
        return $this->photo;
    }

    public function setPhoto($photo){
        $this->photo = $photo;
    }


    public function getAudios(){
        return $this->audio;
    }

    public function setAudios($audio){
        $this->audio = $audio;
    }

    public function getCanSeeAudio(){
        return $this->can_see_audio;
    }

    public function setCanSeeAudio($can_see_audio){
        $this->can_see_audio = $can_see_audio;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }


}
