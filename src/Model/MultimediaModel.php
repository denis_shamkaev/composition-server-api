<?php
namespace Model;
use Entity\Artist;
use Entity\Multimedia;
use League\ColorExtractor\Color;
use League\ColorExtractor\Palette;
use Repository\MultimediaRepository;
use phpQuery;

/**
 * Class MultimediaModel
 * @package Model
 */
class MultimediaModel{
    private $repo;
    private $token;

    public function __construct()
    {
        $this->repo = new MultimediaRepository();
    }

    /**
     * Унифицированная функция курла
     * @param $url
     * @param null $token
     * @return bool|string
     */
    private function getApple($url,$token = null)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: amp-api.music.apple.com",
                "Authorization: Bearer ".$token
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }


    /**
     * @return string|string[]
     */
    public function getTokenByApple(){
        if ($this->token){
            return  $this->token;
        }
        $url    = 'https://music.apple.com/ru/search';
        $musMod = new musicModel();
        $html   = $musMod->file_get_content($url);
        $token2 = [];
        $html   = urldecode($html);
        $token  = preg_match_all('|token\":\".*.\"},\"MUSIC|',$html,$token2);
        $token  = current($token2)[0];
        $token  = str_replace('token":"','',$token);
        $token  = str_replace('"},"MUSIC','',$token);
        $token  = str_replace(' ','',$token);
        if (!$this->token){
            $this->token = $token;
        }
        return $token;
    }

    /**
     * @param $artist
     * @return false|string|string[]
     */
    public function getImageByAppleMusicForArtis($artist){
        $result = $this->repo->getImageByArtistAndSong($artist, null);
        if ($result){
            return $result->getUrlImg();
        }
        $token = $this->getTokenByApple();
        $name_ = urlencode($artist);
        $url   = "https://amp-api.music.apple.com/v1/catalog/ru/search/suggestions?term=$name_&kinds=terms%2CtopResults&types=albums%2Cartists%2Csongs%2Cplaylists%2Cmusic-videos%2Cactivities%2Ctv-episodes%2Ceditorial-items%2Cstations&platform=web&omit[resource]=autos&limit[results%3Aterms]=5&limit[results%3AtopResults]=10";
        $html  = $this->getApple($url,$token);
        $data  = json_decode($html, true);
        if (!isset($data['results']['suggestions']))
        {
            return false;
        }
        $dates = $data['results']['suggestions'];
        unset($dates[0]);
        $tmp = [];
        foreach ($dates as $value) {
            if (!isset($value['content'])) continue;
            if (!isset($value['content']['type']))continue;
            if ($value['content']['type'] !== 'artists') continue;
            if (!isset($value['content']['attributes']))continue;
            if (!isset($value['content']['attributes']['artwork']))continue;
            if (!isset($value['content']['attributes']['artwork']['url']))continue;
            $tmp[] = $value['content']['attributes']['artwork']['url'];
        }
        $img = current($tmp);
        $img = str_replace(['{w}','{h}'],'500',$img);
        $mm = new Multimedia();

        if ($img) {
            $color = $this->getColorImage($img);
            $mm->setUrlImg($img);
            $mm->setSong(" ");
            $mm->setColor($color);
            $mm->setArtist(urldecode($artist));
            $this->repo->addRow($mm);
            }
        return $img;
    }

    /**
     * @param $song
     * @param $artist
     * @param null $isSearch
     * @return mixed|string|string[]
     */
    public function getImageByAppleMusic($song, $artist, $isSearch = null){
        $artist = str_replace('and', '&', $artist);
        $song   = str_replace('#','',$song);
        $result = $this->repo->getImageByArtistAndSong($artist, $song);
        if ($result){
            return $result->getUrlImg();
        }
        $token = $this->getTokenByApple();
        $name_ = urlencode($song);
        $url   = "https://amp-api.music.apple.com/v1/catalog/ru/search/suggestions?term=".$name_."&kinds=terms%2CtopResults&types=albums%2Cartists%2Csongs%2Cplaylists%2Cmusic-videos%2Cactivities%2Ctv-episodes%2Ceditorial-items%2Cstations&platform=web&omit[resource]=autos&limit[results%3Aterms]=5&limit[results%3AtopResults]=10";
        $html  = $this->getApple($url,$token);
        $data  = json_decode($html, true);
        $dates = @$data['results']['suggestions'];
        unset($dates[0]);
        $tmp = [];

        if (empty($dates)){
            return  false;
        }
        foreach ($dates as $value){
            if (!isset($value['content'])) continue;
            if (!isset($value['content']['attributes']))continue;
            if (!isset($value['content']['attributes']['artistName']))continue;
            if (!isset($value['content']['attributes']['name']))continue;
            $tmp[] = $value['content']['attributes'];
        }
        $artist = urldecode($artist);
        $artist = ucfirst($artist);
        $filter_artist = array_filter($tmp, function ($el) use ($song, $artist) {
            return $el['artistName'] == $artist;
        });

        $filter_song = array_filter($tmp, function ($el) use ($song, $artist) {
            return $el['name'] == $song;
        });

        $filter_ar_so = array_filter($tmp, function ($el) use ($song, $artist) {
            return $el['name'] == $song && $el['artistName'] == $artist;
        });
        $tmp = array_merge($filter_artist,$filter_song,$filter_ar_so,$tmp);
        foreach ($tmp as $val){
            foreach ($val as $item){
                if (gettype($item) == 'array') {
                    foreach ($item as $t) {
                        if (gettype($t) == 'array'){
                            continue;
                        }
                        if (strripos($t, 'jpeg') !== FALSE){
                            return str_replace(['{w}','{h}'],'500',$t);
                        }
                    }
                }
            }
            $name = $val['name'];
            if (strripos($name, $song) === FALSE){
                continue;
            }
            $img = isset($val['artwork']['url']) ? $val['artwork']['url'] : '';
            $img = str_replace(['{w}','{h}'],'500',$img);
            return $img;
        }
        return false;
    }

    /**
     * @param $artist
     * @param null $n
     * @return string|string[]
     */
    public function getArtistImage($artist,$n = null){
        try {
            $img = $this->getImageByAppleMusicForArtis($artist);
            if (!$img){
                $img = $this->getArtistByMuzofond($artist);
            }

            if (!$img && !$n){
                $artist = str_replace('feat',  '&', $artist);
                return  $this->getArtistImage($artist, 1);
            }
            if (!$img && $n == 1){
                $artist = str_replace('and',  '&', $artist);
                return  $this->getArtistImage($artist, 2);
            }
            return $img;
        } catch (\Exception $exception){
            dump($exception);
        }

    }

    /**
     * @param $song
     * @param $artist
     * @param null $url
     * @return mixed|string|string[]
     */
    function getImage($song, $artist, $url = null){
            $img = $this->getImageByAppleMusic($song, $artist);
            if (!$GLOBALS['cron']){
                return  $img ? : "https://ruv.hotmo.org/static/images/no-cover-300.jpg";
            }

            if (!$img){
                $img = $this->getImageByHotmo($artist,$song,false);
            }

            return $img ? : false;

    }

        public function getArtistByMuzofond($artist){
            $artist  = str_replace('feat', '&',$artist);
            $artist  = urlencode($artist);
            $url ="https://muzofond.fm/search/{$artist}";
            $mm   = new musicModel();
            $html = $mm->file_get_content($url);
            phpQuery::newDocumentHTML($html);
            /* Получение урл */
            $dates = pq('body > div.container > div.content > div.centerMain > div.span.desktop > div > div.tracksHeader.collectionTop.favoriteConf > div.left > img');
            $img = $dates->attr('data-src');
            $img = str_replace('small','big',$img);
            if (!$img){
                return false;
            }
            return "https://muzofond.fm/{$img}";
        }

        public function getImageByHotmo($artist,$song,$attempt = null): string
        {
        $url  = "https://ruv.hotmo.org/search?q=$song $artist";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        $html = curl_exec($curl);
        preg_match_all('|<div class="track__img" style="background-image: .*.><\/div>|', $html, $arr);
        $img = current($arr);
        if($img){
            $img = str_replace('<div class="track__img" style="background-image: url(\'', 'https://ruv.hotmo.org',$img);
            $img = str_replace('\');"></div>', '',$img);
            $img = str_replace('\');"></div>', '',$img);
            $img = str_replace('150x150', '300x300',$img);
        }
        $img = current($img);
        return $img;
    }

    /**
     * @param $img
     * @return string
     */
    public function getColorImage($img){
        time_nanosleep(0,30000);
        try {
            $palette = Palette::fromFilename($img);
            $topFive = $palette->getMostUsedColors(6);
            $top = [];
            foreach ($topFive as $key => $item) {
                $top[] = $key;
            }
            $color1 = Color::fromIntToHex($top[2]);
            $color2 = Color::fromIntToHex($top[4]);
            return "linear-gradient(to left, $color2 -20%,$color1 50%,$color1 60%,$color2 120%)";
        } catch (\Exception $e){
            dump($e);
            return '';
        }

        }

}