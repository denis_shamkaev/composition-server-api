<?php


namespace Model;

use PDO;
use Repository\UserRepository;
use VK\Client\VKApiClient;
use VK\OAuth\VKOAuth;

class authModel
{

    /**
     * @return \Entity\User
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     * @throws \VK\Exceptions\VKOAuthException
     */
    public function getUserAuthVK()
    {
        $data = $_GET;
        $oauth = new VKOAuth();
        $client_id = 6383380;
        $client_secret = 'myxqfoOru9gKaApIyjus';
        $redirect_uri = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/auth/vkontakte';
        $code = $data['code'];
        $response = $oauth->getAccessToken($client_id, $client_secret, $redirect_uri, $code);
        $access_token = $response['access_token'];

        $vk = new VKApiClient();
        $response = $vk->users()->get($access_token, [
            'fields' => ['city', 'photo_max', 'contacts', 'music', 'can_see_audio'],
        ]);
        if (count($response)) {
            $repo = new UserRepository();
            $user = $repo->getUserByIdVK($response);
            if ($user->getId()) {
                $_SESSION['id'] = $user->getId();
                setcookie("id", (string)$user->getId(), time() + 365 * 24 * 3600);
            } else {
                $user = $repo->creatUserForVK($response);
                $_SESSION['id'] = $user->getId();
                setcookie("id", (string)$user->getId(), time() + 365 * 24 * 3600);
            }
        }
        return $user;

    }

}