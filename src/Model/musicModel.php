<?php

namespace Model;


use Entity\Multimedia;
use phpQuery;
use Repository\MultimediaRepository;
use Repository\MusicRepository;
class musicModel
{

    private $repo;
    /**
     * @var MultimediaModel
     */
    private MultimediaModel $mm;
    /**
     * @var Multimedia
     */
    private Multimedia $mul;

    public function autoComplete($word){
        $repo  = new MusicRepository();
        $words = $repo->getAutoComplete($word);
        $result = [];
        foreach ($words as $word) {
            $text = $word['artist'] . ($word['song'] != 'null' ? " {$word['song']}" : '');
            $result[] = $text;
        }
        $res = array_unique($result, SORT_LOCALE_STRING);
        return json_encode($res);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addMusic($data)
    {
        $db_data = array();
        $db_data['name_song'] = $data['name'];
        $db_data['artist']    = $data['artist'];
        $db_data['duration']  = $data['duration'];
        $db_data['img']       = $data['img'];
        $db_data['like_song'] = (boolean)$data['isLike'];
        $db_data['url']       = $data['url'];
        $db_data['user_id']   = $_SESSION['id'];
        $db_data['date_song'] = date("m.d.y");
        $repo = new MusicRepository();
        return $repo->addMusic($db_data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delMusic($id)
    {
        $repo = new MusicRepository();
        return $repo->delMusic($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getMusicInBase($data){
        $db_data = array();
        $db_data['url']        = $data['url'];
        $db_data['owner_id']   = $_SESSION['id'] ? : 1;
        $repo = new MusicRepository();
        return $repo->getMusic($db_data);
    }

    /**
     * Функция вернет список музыки
     * @param String $url https://muzofond.fm/page=1
     * @param null $otherClass
     * @param null $html
     * @return string
     */
    public function getMusic(string $url, $otherClass = null, $html = null): string
    {
        /*Проверка и возврат кэша*/
        $cache_data = getCache($url);
        $url_cache = $url;
        if($cache_data && !$otherClass && !$GLOBALS['cron']){
            return $cache_data;
        }
        $mm   = new MultimediaRepository();
        $html = $otherClass ? $html :  $this->file_get_content($url);
        phpQuery::newDocumentHTML($html);
        /* Получение урл */
        $dates = pq($otherClass ? 'body > div.container > div.content > div.centerMain > div.span.desktop > div > ul li.item' : 'ul.ajaxContent li.item');
        $result = array();
        foreach ($dates as $data){
            pq($data)->find('noindex')->remove();
            $url    = pq($data)->children('div')->children('ul')->children('li')->attr('data-url');// Получение ссылки
            $artist = pq($data)->children('div')->children('h3')->children('a')->children('.artist')->html() ? :
                pq($data)->children('div')->children('h3')->children('.artist')->html() ;
            $song   = pq($data)->children('div')->children('h3')->children('a')->children('.track')->html() ? :
                pq($data)->children('div')->children('h3')->children('.track')->html();
            $time   = pq($data)->children('.duration.enemy')->html();
            $artist = str_replace('&amp;', 'and',$artist);
            $song   = preg_replace("/\(.*.\)/",'', $song);
            if ($GLOBALS['cron']){
                if (!$this->repo){
                    $this->repo = new MultimediaRepository();
                    $this->mm   = new MultimediaModel();
                    $this->mul  = new Multimedia();
                }
                $img = $this->mm->getArtistImage($artist);
                $apl = $this->mm->getImage($song,$artist,$url);
                $img = $apl ? $apl : $img;
                $field = $this->repo->getImageByArtistAndSong($artist,$song);
                if ($img && $field === false && $img != "https://ruv.hotmo.org/static/images/no-cover-150.jpg"){
                    $mm = $this->mul;
                    if ($field === false){
                        $color = $this->mm->getColorImage($img);
                        $mm->setUrlImg($img);
                        $mm->setSong(str_replace("'",'',$song));
                        $mm->setUrlMp3($url);
                        $mm->setColor($color);
                        $mm->setArtist(urldecode($artist));
                        $this->repo->addRow($mm);
                        dump(convert(memory_get_usage(true)).' | '.$artist .' - '.$song .' | '. $img); // 123 kb
                    }
                }
            } else {
                $data = $mm->getImageByArtistAndSong($artist,$song);
                $img  = $data ?  $data->getUrlImg() : 'img/no-cover-300-original.jpg';
            }

            array_push($result, [
                'url'      => $url,
                'artist'   => $artist,
                'name'     => $song,
                'duration' => $time,
                'img'      => $img,
            ]);
        }
        if(!$result && !$otherClass){
            return $this->getMusic($url,true, $html);
        }
        $result = json_encode($result);
        /*Создание кэша*/
        setCache($url_cache, $result);
        gc_collect_cycles();
        return $result;
    }

    /**
     * Функция вернет список жанров
     * @param String $url
     * @return array|string
     */
    public function getGenresFromServer($url = null)
    {
        $url   = $url ? $url : 'https://muzofond.fm/popular';
        $name  = $url;
        $cache = getCache($name);
        if ($cache){
            return  $cache;
        }
        $html = $this->file_get_content($url);
        phpQuery::newDocumentHTML($html);
        $genres = pq('body > div.container > div.content > div.centerMain > div.span.desktop > div > ul > div > li > a');
        $result = [];
        foreach ($genres as $genre){
            $g  = pq($genre);
            $url = str_replace('https://muzofond.fm/', '' ,$g->attr('href'));
            $result[$url] = $this->clearest($g->html());
        }

        $result = json_encode($result,JSON_FORCE_OBJECT);
        setCache($name,$result);
        return $result;
    }

    /**
     * @param null $url
     * @return false|string
     */
    public function getGenres($url = null){
        $model  = new genresModel();
        return $model->getGenres();
    }



    /**
     * @param $name
     * @return string
     */
    public function getArtistVk($name): string
    {
        $vk_model = new vkModel();
        return $vk_model->getArtistByName($name);
    }


    /**
     * @param $str
     * @return mixed
     */
    function utf8_str_split($str): array
    {
        // place each character of the string into and array
        $split=1;
        $array = array();
        for ( $i=0; $i < strlen( $str ); ){
            $value = ord($str[$i]);
            if($value > 127){
                if($value >= 192 && $value <= 223)
                    $split=2;
                elseif($value >= 224 && $value <= 239)
                    $split=3;
                elseif($value >= 240 && $value <= 247)
                    $split=4;
            }else{
                $split=1;
            }
            $key = NULL;
            for ( $j = 0; $j < $split; $j++, $i++ ) {
                $key .= $str[$i];
            }
            array_push( $array, $key );
        }
        return $array;
    }

    /**
     * Функция вырезки
     * @param <string> $str
     * @return <string>
     */
    function clearest($str){
        $sru = 'ёйцукенгшщзхъфывапролджэячсмитьбю';
        $s1 = array_merge($this->utf8_str_split($sru), $this->utf8_str_split(strtoupper($sru)), range('A', 'Z'), range('a','z'), range('0', '9'), array('&',' ','#',';','%','?',':','(',')','-','_','=','+','[',']',',','.','/','\\'));
        $codes = array();
        for ($i=0; $i<count($s1); $i++){
            $codes[] = ord($s1[$i]);
        }
        $str_s = $this->utf8_str_split($str);
        for ($i=0; $i<count($str_s); $i++){
            if (!in_array(ord($str_s[$i]), $codes)){
                $str = str_replace($str_s[$i], '', $str);
            }
        }
        return $str;
    }



    /**
     * Унифицированная функция курла
     * @param string $url url куда обращаться
     * @return bool|string
     */
    public function file_get_content(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla / 5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit / 537.36 (KHTML, как Gecko) Chrome / 80.0.3987.163 Safari / 537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $html = curl_exec($ch);
        curl_close($ch);
        return $html;
    }

}
