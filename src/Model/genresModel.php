<?php
namespace Model;
use Repository\GenresRepository;

/**
 * Class genresModel
 * @package Model
 */
class genresModel{
    /**
     * @var GenresRepository
     */
    private $repo;

    /**
     * genresModel constructor.
     */
    public function __construct()
    {
        $this->repo = new GenresRepository();
    }

    /**
     * @return bool|\Entity\Genre
     */
    public function getGenres(){
        return $this->repo->getGenres();
    }

    /**
     * @param $data
     */
    public function setGenres($data){
        $this->repo->UpdateRow($data);
    }

    /**
     * @param $data
     */
    public function addGenre($data){
        $this->repo->addRow($data);
    }

}