<?php
namespace Model;

use Repository\RadioRepository;

/**
 * Class radioModel
 * @package Model
 */
class radioModel{
    /**
     * @var RadioRepository
     */
    private $repo;

    /**
     * genresModel constructor.
     */
    public function __construct()
    {
        $this->repo = new RadioRepository();
    }

    /**
     * @return array
     */
    public function getRadios(){
        return $this->repo->getRadios();
    }

    /**
     * @param $data
     */
    public function setRadio($data){
        $this->repo->UpdateRow($data);
    }

    /**
     * @param $data
     */
    public function addRadio($data){
        $this->repo->addRow($data);
    }

}