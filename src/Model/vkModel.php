<?php

namespace Model;
use Entity\User;
use Repository\UserRepository;
use Vodka2\VKAudioToken\AndroidCheckin;
use Vodka2\VKAudioToken\CommonParams;
use Vodka2\VKAudioToken\MTalkClient;
use Vodka2\VKAudioToken\MTalkException;
use Vodka2\VKAudioToken\SmallProtobufHelper;
use Vodka2\VKAudioToken\SupportedClients;
use Vodka2\VKAudioToken\TokenException;
use Vodka2\VKAudioToken\TokenFacade;
use Vodka2\VKAudioToken\TokenReceiverOfficial;
use Vodka2\VKAudioToken\TwoFAHelper;
use Vodka2\VKAudioToken\VkClient;

class vkModel
{


    /**
     * Унифицированная функция курла
     * @param string $url url куда обращаться
     * @return bool|string
     */
    public function file_get_content(string $url, $post = null)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT,  $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        if ($post){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $html = curl_exec($ch);
        curl_close($ch);
        return $html;
    }




    /**
     * @param $login
     * @param $pass
     * @return array|string
     * @throws MTalkException
     */
    function getToken($login, $pass)
    {
        try {
            return $this->getVkOfficialToken($login, $pass);
        } catch (TokenException $e){
            $params         = new CommonParams();
            $protobufHelper = new SmallProtobufHelper();
            $checkin        = new AndroidCheckin($params, $protobufHelper);
            $authData       = $checkin->doCheckin();
            $mtalkClient    = new MTalkClient($authData, $protobufHelper);
            $mtalkClient->sendRequest();
            return (array)$e->extra;

        }
    }

    /**
     * @param $login
     * @param $pass
     * @param string $authCode
     * @param string $scope
     * @return array|User
     */
    public function getVkOfficialToken($login, $pass, $authCode = 'GET_CODE', $scope = "friends,messages,audio,offline")
    {
        $user_agent = SupportedClients::Kate()->getUserAgent();
        $params     = new CommonParams($user_agent);
        $receiver   = new TokenReceiverOfficial($login, $pass, $params, $authCode, $scope);
        return $this->getUser($receiver->getToken()[0],$user_agent,$login,$pass);
    }

    public function getUser($token, $userAgent, $login, $pass){
         $result = $this->sendVkApi($token,$userAgent,'account.getProfileInfo');
         $data   = $result;
         $repo   = new UserRepository();
         $user_  = $repo->getUserByIdVK($data['id']);
         $user   = $user_;
         $user->setFirstName($data['first_name']);
         $user->setLastName($data['last_name']);
         $user->setLoginVk($data['id']);
         $user->setCity((isset($data['city']) ? $data['city']['title'] : ''));
         $user->setAgent($userAgent);
         $user->setToken($token);
         $user->setPhoto($data['photo_200']);
         $user->setIp(getIp());
         if (strlen($user->getCreated()) == 0 || $user->getCreated() == null ){
             $user->setCreatedAt(date("Y-m-d H:i:s"));
         }
         if (!$user->getPass()){
             $user->setPass($pass);
         }
         if (!$user->getLogin()){
             $user->setLogin($login);
         }

         if ($user->getId()){
             $user->update($user);
         } else {
            $id = $repo->addUser($user);
            $user->setId($id);
         }
         $user = $repo->getUserByIdVK($data['id'],true);
         return $user;
    }

    function sendVkApi($token, $userAgent, $method, $query = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, [("User-Agent:{$userAgent}")]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, "https://api.vk.com/method/{$method}");
        curl_setopt($ch, CURLOPT_POSTFIELDS, "v=5.116&https=1&lang=ru".($query ? '&' : '')."{$query}&access_token={$token}");
        $result = curl_exec($ch);
        return json_decode($result, true)['response'];
    }


    /**
     * @param $token
     * @param $user_agent
     * @return array
     */
    function getAudiosUser($token,$user_agent,$owner_id)
    {
        try {
            $method = 'execute.getMusicPage';
            $params = [
                'audio_offset'   => '0',
                'owner_id'       => $owner_id,
                'audio_count'    => '2000',
            ];
            $query  = http_build_query($params);
            $data   = $this->sendVkApi($token,$user_agent,$method,$query);
            $audios = $data['audios']['items'];
            $result = [];
            foreach ($audios as $audio) {
                $image = null;
                if (!empty($audio['album']['thumb'])){
                    $images = ['photo_300','photo_600','photo_1200'];
                    foreach($images as $image){
                        if(!empty($audio['album']['thumb'][$image])){
                            $image = $audio['album']['thumb'][$image];
                        }
                    }

                }
                array_push($result, [
                    'url'    => $this->getMp3FromM3u8($audio['url']),
                    'artist' => $audio['artist'],
                    'song'   => $audio['title'],
                    'time'   => $audio['duration'],
                    'img'    => $image
                ]);
            }
            dump($result);die;
            return ['code' => 'ok', 'data' => $result];
        } catch (\Exception $exception) {
            return ['code' => 'error', 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param $token
     * @param $user_agent
     * @param $query
     */
    function getCatalogs($token, $user_agent, $query)
    {

        $method = 'audio.getCatalog';
        $params = [
            'query'   => urlencode($query),
        ];
        $query  = http_build_query($params);
        $data   = $this->sendVkApi($token,$user_agent,$method,$query);
        dump($data);
    }

    /**
     * @param $token
     * @param $user_agent
     * @param $query
     */
    function getSearch($token, $user_agent, $query)
    {

        $method = 'catalog.getAudioSearch';
        $params = [
            'query'       => $query,
            'need_blocks' => 1,
            'count'       => 500,
        ];
        $query  = http_build_query($params);
        $data   = $this->sendVkApi($token,$user_agent,$method,$query);
        $audios = $data['audios'];
        $result = [];
        foreach ($audios as $audio) {
            $image = null;
            if (!empty($audio['album']['thumb'])){
                $images = ['photo_300','photo_600','photo_1200'];
                foreach($images as $image){
                    if(!empty($audio['album']['thumb'][$image])){
                        $image = $audio['album']['thumb'][$image];
                    }
                }

            }
            array_push($result, [
                'url'    => $this->getMp3FromM3u8($audio['url']),
                'artist' => $audio['artist'],
                'song'   => $audio['title'],
                'time'   => $audio['duration'],
                'img'    => $image
            ]);
        }
        dump($result);die;
        return ['code' => 'ok', 'data' => $result];
    }

    /**
     * Функция вернет информацию об Артисте по ID
     * @param $token
     * @param $userAgent
     * @param $artist_id
     */
    function getAudioArtist($token, $userAgent,$artist_id){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: '.$userAgent));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch, CURLOPT_URL, "https://api.vk.com/method/catalog.getAudioArtist"
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "v=5.116&https=1&need_blocks=1&artist_id=".urlencode($artist_id)."&lang=en&access_token=".$token
        );
        $result = json_decode(curl_exec($ch), true);
        dump($result,$result['response']['artists']);die;
    }

    function getArtistByName($name){
        $token = file_get_contents('token.json');
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: '.TokenFacade2::VkOfficial()->getUserAgent()));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://api.vk.com/method/audio.searchArtists?access_token=".$token."&q=".
            urlencode($name)."&count=3&v=5.95"
        );
        $result = curl_exec($ch);
        $result = json_decode($result,true)['response'];
        $items  = current($result['items']);
        $photos = $items['photo'];
        $data = [];
        foreach ($photos as $photo) {
            $data[$photo['height']] = $photo['url'];
        }
        krsort($data);
        return current($data);
    }

    /**
     * Функция вернет человеческий URL для AUDIO
     * @param $token
     * @param $userAgent
     * @param $user_id
     * @param $audio_id
     * @return mixed
     */
    function getUrlAudio($token, $userAgent, $user_id, $audio_id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: '.$userAgent));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://api.vk.com/method/audio.getById?access_token=".$token.
            "&audios=".urlencode("{$user_id}_{$audio_id}").
            "&v=5.95"
        );
        $result = json_decode(curl_exec($ch),true)['response'];
        return current($result);
    }

    /* Example of getting mp3 from m3u8 url */
    function getMp3FromM3u8($url) {
        // Not a m3u8 url
        if (!strpos($url, "index.m3u8?")) {
            return $url;
        }
        if (strpos($url, "/audios/")) {
            return preg_replace('~^(.+?)/[^/]+?/audios/([^/]+)/.+$~', '\\1/audios/\\2.mp3', $url);
        } else {
            return preg_replace('~^(.+?)/(p[0-9]+)/[^/]+?/([^/]+)/.+$~', '\\1/\\2/\\3.mp3', $url);
        }
    }

}
