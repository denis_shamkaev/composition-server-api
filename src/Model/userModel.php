<?php
namespace Model;
use Repository\UserRepository;

class userModel {
    public function __construct($id = null)
    {

    }

    function getUserById($id, $isArray)
    {
        $repo = new UserRepository();
        return $repo->getUserById($id,$isArray);
    }
}