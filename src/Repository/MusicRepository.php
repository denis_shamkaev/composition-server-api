<?php

namespace Repository;

use Entity\User;
use PDO;
use PDOException;
use System\App;

class MusicRepository
{
    //CREATE TABLE `black`.`music` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `artist` VARCHAR(255) NOT NULL , `duration` VARCHAR(255) NOT NULL , `img` VARCHAR(255) NOT NULL , `isLike` INT NOT NULL , `url` TEXT NOT NULL , `owner_id` INT NOT NULL , `date` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
    function getAutoComplete($word){
        $pdo = App::Mysql();
        $word = strtolower($word);
        $word = urldecode($word);
        $query="SELECT * FROM multimedia WHERE LOWER(artist) LIKE '%{$word}%' OR LOWER(song) LIKE '%{$word}%' LIMIT 10";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            dump($e->getMessage());
        }
    }


    function getMyMusic()
    {
        $pdo  = App::MySql();
        $stmt = $pdo->prepare("SELECT * FROM `music` WHERE `owner_id` = :id  ORDER BY `music`.`id` DESC");
        $stmt->bindValue(':id', $_SESSION['id'], PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    function addMusic($data)
    {
        $pdo = App::MySql();
        $query = "INSERT INTO `music` (`id`, `song`, `artist`, `time`, `img`, `isLike`, `url`, `owner_id`, `date`) VALUES (NULL , :name_song , :artist, :duration, :img, :like_song, :url, :user_id, :date_song)";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute($data);
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
        return $pdo->lastInsertId();
    }

    function UpdateMusic($data)
    {
        $pdo = App::MySql();
        $query = "UPDATE `music` SET `url`=:url WHERE `id`=:id";
        dump($data);
        try {
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(':id', $data['id'], PDO::PARAM_INT);
            $stmt->bindValue(':url', $data['url'], PDO::PARAM_STR);
            return $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }


    function getAudiosNotUrl(){
        $pdo  = App::MySql();
        $stmt = $pdo->prepare("SELECT * FROM `music` WHERE `url` = :url");
        $stmt->bindValue(':url', 'get');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }




    function delMusic($id)
    {
        $pdo = App::MySql();
        $query = "DELETE FROM `music` WHERE `music`.`id` = ?";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute(array($id));
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function getMusic($data){
        $pdo = App::MySql();
        $query = "SELECT *  FROM `music` WHERE `url` LIKE :url AND `owner_id` = :owner_id";
        $stmt = $pdo->prepare($query);
        $stmt->execute($data);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param User $user
     * @return array
     */
    function getUserArray($user)
    {
        return [
            'login_vk'   => (string)$user->getLoginVk(),
            'login_inst' => $user->getLoginInst(),
            'phone'      => $user->getPhone(),
            'can_see_audio' => (string)$user->getCanSeeAudio(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'city' => $user->getCity(),
            'audios' => $user->getAudios(),
            'photo' => $user->getPhoto(),
            'crated' => $user->getCreated(),
            'updated' => $user->getUpdated()
        ];
    }
}