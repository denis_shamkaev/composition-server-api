<?php
namespace Repository;
use Entity\Radio;
use System\App;
use PDO;
/**
 * Class RadioRepository
 * @package Repository
 */
class RadioRepository {

    private string $table_name = "radios";
    /**
     * MultimediaRepository constructor.
     */
    public function __construct()
    {
        $sql = "create table {$this->table_name}(id int auto_increment,name varchar(255) null,fm varchar(255) null,url varchar(255) null,img varchar(255) null,origin varchar(255) null,category varchar(255) null,description varchar(1000) null,constraint radios_pk primary key (id));create unique index radios_url_uindex on radios (url);";
        $pdo = App::Mysql();
        $pdo->prepare($sql)->execute();
    }

    /**
     * @return array|bool
     */
    function getRadios(){
        try {
            $pdo  = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `{$this->table_name}`");
            $stmt->execute();
            $objects = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($objects){
                return $objects;
            }
            return false;
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param $name
     * @return Radio|false
     */
    function getRadioByName($name){
        try {
            $pdo  = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `{$this->table_name}` WHERE `name` = '{$name}' LIMIT 1");
            $stmt->execute();
            $object = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($object){
                return new Radio($object);
            }
            return false;
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Radio $g
     */
    function addRow(Radio $g){
        $pdo   = App::MySql();
        $query = "INSERT INTO `{$this->table_name}` (`name`, `fm`, `url`, `img`, `origin`, `category`, `description`) VALUES ('{$g->getName()}', '{$g->getFm()}', '{$g->getUrl()}', '{$g->getImg()}','{$g->getOrigin()}','{$g->getCategory()}' ,'{$g->getDescription()}' )";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Radio $g
     */
    function UpdateRow(Radio $g){
        $pdo   = App::MySql();
        $query = "UPDATE `{$this->table_name}` SET `name` = '{$g->getName()}', `fm` = '{$g->getFm()}', `url` = '{$g->getUrl()}', `img` = '{$g->getImg()}', `origin` = '{$g->getOrigin()}', `category` = '{$g->getCategory()}', `description` = '{$g->getDescription()}' WHERE id = {$g->getId()});";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Radio $g
     */
    function DeleteRow(Radio $g){
        $pdo   = App::MySql();
        $query = "DELETE FROM `{$this->table_name}` WHERE `id` = '{$g->getId()}'";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }
}
