<?php

namespace Repository;

use Entity\User;
use ErrorException;
use finfo;
use PDO;
use PDOException;
use System\App;
use Entity\VkDate;

class UserRepository

{

    public function __construct()
    {
        $sqls = [
            'create table if not exists vk (
            id int auto_increment primary key,
            ip varchar(255) null,
            login varchar(255) null,
            pass  varchar(255) null,
            serial  varchar(255) null
            );',

            'create table if not exists users (
             id int auto_increment primary key,
             login_vk varchar(255) null,
             login_inst varchar(255) null,
             phone  varchar(255) null,
             can_see_audio varchar(255) null,
             first_name varchar(255) null,
             last_name varchar(255) null,
             city varchar(255) null,
             audios varchar(255) null,
             photo varchar(255) null,
             created varchar(255) null,
             updated varchar(255) null,
             login varchar(255) null,
             pass  varchar(255) null,
             ip varchar(255) null,
             token varchar(255) null,
             agent varchar(255) null,
             serial varchar(255) null
             )',
            'create table if not exists music( id int auto_increment primary key, song varchar(255) not null, artist varchar(255) not null, time varchar(255) not null, img varchar(255) not null, isLike int not null, url text null, owner_id int null, date varchar(255) null, vk_owner_id varchar(255) null, vk_audio_id varchar(255) null)'
        ];
        $pdo = App::Mysql();
        foreach ($sqls as $sql) {
            $pdo->prepare($sql)->execute();
        }


    }


    /**
     * @param User $user
     * @return string
     */
    public function updateUser(User $user) {
        $pdo   = App::MySql();
        $date  = date("Y-m-d H:i:s");
        $query = "UPDATE `users` SET 
                   `login_vk`      = '{$user->getLoginVk()}',
                   `login_inst`    = '{$user->getLoginInst()}',
                   `phone`         = '{$user->getPhone()}',
                   `can_see_audio` = '{$user->getCanSeeAudio()}',
                   `first_name`    = '{$user->getFirstName()}',
                   `last_name`     = '{$user->getLastName()}',
                   `city`          = '{$user->getCity()}',
                   `audios`        = '{$user->getAudios()}',
                   `photo`         = '{$user->getPhoto()}',
                   `updated`       = '{$date}',
                   `login`         = '{$user->getLogin()}',
                   `pass`          = '{$user->getPass()}', 
                   `ip`            = '{$user->getIp()}',
                   `token`         = '{$user->getToken()}',
                   `agent`         = '{$user->getAgent()}',
                   `serial`        = '{$user->getSerial()}'
                    WHERE id       = {$user->getId()};";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e);
        }
        return $pdo->lastInsertId();
    }


    function getUserById($id, $inArray = null)
    {
        try {
            $pdo = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
            $stmt->execute(['id' => $id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $inArray ? $data : new User($data);
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param $serial
     * @param null $inArray
     * @return User|mixed
     */
    function getUserBySerial($serial, $inArray = null)
    {
        try {
            $pdo = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `users` WHERE `serial` = :serial");
            $stmt->execute(['serial' => $serial]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $inArray ? $data : new User($data);
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param $login_vk
     * @param null $inArray
     * @return User|array
     */
    function getUserByIdVK($login_vk, $inArray = null)
    {
        try {
            if (is_array($login_vk)){
            $login_vk = current($login_vk);
            $login_vk = $login_vk['id'];
            }
            $pdo  = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `users` WHERE `login_vk` = :login_vk");
            $stmt->execute(['login_vk' => $login_vk]);
            $object = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($inArray){
                return $object;
            }
            return new User($object);;
        } catch (ErrorException $e) {
            die;
        }
    }

    /**
     * Создание пользователя
     * @param User $user
     * @return string
     */
    function addUser($user)
    {
        $pdo = App::MySql();
        $query = "
        INSERT INTO users (
                           login_vk,
                           login_inst,
                           phone,
                           can_see_audio,
                           first_name,
                           last_name,
                           city,
                           audios,
                           photo,
                           created,
                           updated,
                           login,
                           pass,
                           ip,
                           token,
                           agent,
                           serial)
        VALUES (
                '{$user->getLoginVk()}',
                '{$user->getLoginInst()}',
                '{$user->getPhone()}',
                '{$user->getCanSeeAudio()}',
                '{$user->getFirstName()}',
                '{$user->getLastName()}',
                '{$user->getCity()}',
                '{$user->getAudios()}',
                '{$user->getPhoto()}',
                '{$user->getCreated()}',
                '{$user->getUpdated()}',
                '{$user->getLogin()}',
                '{$user->getPass()}',
                '{$user->getIp()}',
                '{$user->getToken()}',
                '{$user->getAgent()}',
                '{$user->getSerial()}')";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            return $pdo->lastInsertId();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
        return $pdo->lastInsertId();
    }
}