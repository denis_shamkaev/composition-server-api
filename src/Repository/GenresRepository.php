<?php
namespace Repository;
use Entity\Genre;
use System\App;
use PDO;
/**
 * Class GenresRepository
 * @package Repository
 */
class GenresRepository{
    private string $table_name = "genres";
    /**
     * MultimediaRepository constructor.
     */
    public function __construct()
    {
        $sql = "create table if not exists {$this->table_name} (id int auto_increment, name varchar(255) null, url varchar(255) null, img varchar(255) null, color varchar(255) null, other varchar(255) null, constraint genres_pk primary key (id)); create unique index genres_url_uindex on genres (url);";
        $pdo = App::Mysql();
        $pdo->prepare($sql)->execute();
    }

    /**
     * @return array|bool
     */
    function getGenres(){
        try {
            $pdo  = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `{$this->table_name}`");
            $stmt->execute();
            $objects = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($objects){
                return $objects;
            }
            return false;
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param $name
     * @return Genre|false
     */
    function getGenreByName($name){
        try {
            $pdo  = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `{$this->table_name}` WHERE `name` = '{$name}' LIMIT 1");
            $stmt->execute();
            $object = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($object){
                return new Genre($object);
            }
            return false;
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Genre $g
     */
    function addRow(Genre $g){
        $pdo   = App::MySql();
        $query = "INSERT INTO `{$this->table_name}` (`name`, `url`, `img`, `color`, `other`) VALUES ('{$g->getName()}', '{$g->getUrl()}', '{$g->getImg()}','{$g->getColor()}','{$g->getOther()}' )";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Genre $g
     */
    function UpdateRow(Genre $g){
        $pdo   = App::MySql();
        $query = "UPDATE `{$this->table_name}` SET `name` = '{$g->getName()}', `url` = '{$g->getUrl()}', `img` = '{$g->getImg()}', `color` = '{$g->getColor()}', `other` = '{$g->getOther()}' WHERE id = {$g->getId()}";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Genre $g
     */
    function DeleteRow(Genre $g){
        $pdo   = App::MySql();
        $query = "DELETE FROM `{$this->table_name}` WHERE `id` = '{$g->getId()}'";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }
}
