<?php
namespace Repository;
use Entity\Artist;
use Entity\Multimedia;
use System\App;
use PDO;
/**
 * Class MultimediaRepository
 * @package Repository
 */
class MultimediaRepository{
    /**
     * MultimediaRepository constructor.
     */
    public function __construct()
    {
        $pdo = App::Mysql();
        $sql = 'create table if not exists multimedia (id int auto_increment primary key,artist varchar(255) null,song varchar(255) null,url_img  varchar(255) null,url_mp3  varchar(255) null,color  varchar(255) null);';
        $pdo->prepare($sql)->execute();
        $sql = 'create table if not exists multimedia2 (id int auto_increment primary key,artist varchar(255) null,song varchar(255) null,url_img  varchar(255) null,url_mp3  varchar(255) null); create unique index url_img_uindex on multimedia2 (url_img);';
        $pdo->prepare($sql)->execute();
        $sql = 'create table if not exists multimedia_artist (id int auto_increment primary key,artist varchar(255) null, url_img  varchar(255), color  varchar(255) null);';
        $pdo->prepare($sql)->execute();


    }

    /**
     * @param $artist
     * @param $song
     * @return Multimedia|false
     */
    function getImageByArtistAndSong($artist, $song = null){
        try {
            $pdo  = App::MySql();
            $song = str_replace("'",'',$song);
            $song = $song ? $song : ' ';
            $stmt = $pdo->prepare("SELECT * FROM `multimedia` WHERE `artist` = '{$artist}' and `song` = '{$song}' LIMIT 1");
            $stmt->execute();
            $object = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($object){
                return new Multimedia($object);
            }
            return false;
        } catch (PDOException $e) {
            dump($e);
            die;
        }
    }
    /**
     * @param $artist
     * @return Artist|false;
     */
    function getImageByArtist($artist){
        try {
            $pdo  = App::MySql();
            $stmt = $pdo->prepare("SELECT * FROM `multimedia_artist` WHERE `artist` = '{$artist}' LIMIT 1");
            $stmt->execute();
            $object = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($object){
                return new Artist($object);
            }
            return false;
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * id
     * artist
     * song
     * url_img
     * url_mp3
     * @param Multimedia $mm
     */
    function addRow(Multimedia $mm){
        $pdo   = App::MySql();

//        $query = "INSERT INTO `multimedia` (`artist`, `song`, `url_img`, `url_mp3`, `color`) VALUES ('{$mm->getArtist()}', '{$mm->getSong()}', '{$mm->getUrlImg()}','{$mm->getUrlMp3()}','{$mm->getColor()}' )";
        $query = "INSERT INTO `multimedia` (`artist`, `song`, `url_img`, `url_mp3`, `color`) VALUES (:artist, :song, :url_img, :url_mp3, :color)";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute($mm->toArray());
        } catch (PDOException $e) {
            dump($e->getMessage());
        }
    }

    /**
     * @param Artist $mm
     */
    function addArtist(Artist $mm){
        $pdo   = App::MySql();
        $query = "INSERT INTO `multimedia_artist` (`artist`, `url_img`, `color`) VALUES (:artist, :url_img, :color);";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute($mm->toArray());
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * id
     * artist
     * song
     * url_img
     * url_mp3
     * @param Multimedia $mm
     */
    function addRowNoImage(Multimedia $mm){
        $pdo   = App::MySql();
        $query = "INSERT INTO `multimedia2` (`artist`, `song`, `url_img`, `url_mp3`) VALUES ('{$mm->getArtist()}', '{$mm->getSong()}', '{$mm->getUrlImg()}','{$mm->getUrlMp3()}' )";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Multimedia $mm
     */
    function UpdateRow(Multimedia $mm){
        $pdo   = App::MySql();
        $query = "UPDATE `multimedia` SET `artist` = '{$mm->getArtist()}', `song` = '{$mm->getSong()}', `url_img` = '{$mm->getUrlImg()}', `url_mp3` = '{$mm->getUrlMp3()}' WHERE id = {$mm->getId()});";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }

    /**
     * @param Multimedia $mm
     */
    function DeleteRow(Multimedia $mm){
        $pdo   = App::MySql();
        $query = "DELETE FROM `multimedia` WHERE `id` = '{$mm->getId()}'";
        try {
            $stmt = $pdo->prepare($query);
            $stmt->execute();
        } catch (PDOException $e) {
            dump($e->getMessage());
            die;
        }
    }
}
