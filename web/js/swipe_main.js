$(function() {
    //Enable swiping...
    $("#test").swipe( {
        //Generic swipe handler for all directions
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            if(direction == 'up' && duration >= 1000){
                $('#player_player').animate({'height': 'show'});
            }
            if(direction == 'down' && duration >= 1000){
                $('#player_player').animate({'height': 'hide'});
            }
        },
        //Default is 75px, set to 0 for demo so any distance triggers swipe
        threshold:75
    });
});