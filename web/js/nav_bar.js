jQuery(document).ready(function ($) {

    var nav_play = $('#nav_play');
    var nav_list = $('#nav_list');
    var nav_cloud = $('#nav_cloud');
    var player = $('#player_player');
    var active = 'active-icon';
    var body = $('body');

    /**
     * Если нажата кнопка плеера
     */
    nav_play.click(function (err) {
        player.animate({'height': 'toggle'});

        if (nav_play.hasClass(active)) {
            nav_list.addClass(active);
            nav_play.removeClass(active);
            body.css({'overflow':'visible', 'position':'static'});


        } else {
            $('.active-icon').removeClass(active);
            nav_play.addClass(active);
            body.css({'overflow':'hidden', 'position':'fixed'});
        }

    });

    /**
     * Если нажата копка лист
     */
    nav_list.click(function (err) {
        player.animate({'height': 'hide'});

        if (nav_play.hasClass(active)) {
            nav_list.addClass(active);
            nav_play.removeClass(active);
            body.css({'overflow':'visible', 'position':'static'});
        } else {
            $('.active-icon').removeClass(active);
            nav_list.addClass(active);
            body.css({'overflow':'visible', 'position':'static'});

        }

    });

    /**
     * Если нажата кнопка Облочко
     */
    nav_cloud.click(function (err) {
        player.animate({'height': 'hide'});
        nav_play.removeClass(active);

        if (nav_play.hasClass(active)) {
            nav_cloud.addClass(active);
            body.css({'overflow':'hidden', 'position':'fixed'});
        } else {
            $('.active-icon').removeClass(active);
            nav_cloud.addClass(active);
            body.css({'overflow':'visible', 'position':'static'});


        }

    })

});