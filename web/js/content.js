jQuery(document).ready(function ($) {
    $("#search").submit(function () {
        const form = $(this)
        let content = $("#content-row")
        $.ajax({
            url: '/index.php',
            method: 'POST',
            data: form.serialize(),
            success: function (data) {
                content.slideUp()
                $("#content").append(data).slideDown("slow")
                setTimeout(function () {
                    content.remove()
                }, 2000)
                $("#search-input").val('')
            }
        });
        return false
    });
});