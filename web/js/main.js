jQuery(document).ready(function ($) {

    viewportmeta = document.querySelector('meta[name="viewport"]'),
        viewportmeta.content = 'user-scalable=NO, width=device-width, initial-scale=1.0'

    Splayer = document.getElementById("player")
    audio = $('#player')
    last_song = $('#last-song')
    player_song_name = $('#player_song_name')
    player_song_artist = $('#player_song_artist')
    play_pause = $('#play-pause')
    song_name = ''
    song_artist = ''
    currer_song = -2
    random = false
    repeat = false
    playing = stop
    active = 'active'
    pause = "M5.5 3.5A1.5 1.5 0 0 1 7 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5zm5 0A1.5 1.5 0 0 1 12 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5z"
    play = "M10.804 8L5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z"


    $('body').on('click', '#shuffle', function () {
        random = random ? false : true
    })

    $('body').on('click', '#repeat', function () {
        repeat = repeat ? false : true
    })

    $('body').on('click', "#previous", function ($e) {
        var id = last_song.val();
        if (random) {
            id = Math.floor(Math.random() * 101)
        }
        id--;
        if (id < 0) id = 0;
        const musick = $('#song-' + id),
            url = musick.attr('data-url'),
            name = musick.attr('data-name'),
            artist = musick.attr('data-track');
        clearPlayer();
        updateAudion(id, url, name, artist)
        return false;
    });

    $('body').on('click', "#next", function ($e) {
        var id = last_song.val();
        if (random) {
            id = Math.floor(Math.random() * 99)
        }
        id++;
        var musick = $('#song-' + id),
            url = musick.attr('data-url'),
            name = musick.attr('data-name'),
            artist = musick.attr('data-track');
        clearPlayer();
        updateAudion(id, url, name, artist)
        return false;
    });

    $("body").on('click', "#play-pause", function () {
        var id = last_song.val();
        if (id < 0) id = 0;
        var musick = $('#song-' + id),
            url = musick.attr('data-url'),
            name = musick.attr('data-name'),
            artist = musick.attr('data-track');
        clearPlayer();
        updateAudion(id, url, name, artist)


        if ($(this).attr('class') == 'amplitude-paused') {
            alert('play_pause');
            $(this).removeClass('amplitude-paused').addClass('amplitude-playing')
        } else {
            alert('else play_pause');
            $(this).removeClass('amplitude-playing').addClass('amplitude-paused')
        }
        return false;
    });


    $("body").on('click', ".button_audio", function () {
        var url, id, song_name_t, song_artist_t;
        url = this.getAttribute('data-url');
        id = this.getAttribute('data-song-id');
        song_name_t = this.getAttribute('data-name');
        song_artist_t = this.getAttribute('data-track');
        song_artist = song_artist_t;
        song_name = song_name_t;
        clearPlayer();
        updateAudion(id, url);
        cacheTitle(song_name_t, song_artist_t);
        cacheArtist(song_name_t, song_artist_t)
    });


    $('audio').on('ended', function (err) {
        var id = last_song.val();
        if (!repeat) {
            id++;
        }
        var musick = $('#song-' + id),
            url = musick.attr('data-url'),
            name = musick.attr('data-name'),
            artist = musick.attr('data-track');

        if (repeat) {
            updateAudionRepiat(id, url, name, artist)
            return false;
        }
        clearPlayer();
        updateAudion(id, url, name, artist)
    });


    function clearPlayer() {
        if (last_song !== currer_song) {
            var played = $("." + active)
            var string_run = $(".marquee")
            played.removeClass(active)
            string_run.removeClass("marquee")
            var button = $('#status-' + last_song.val())
            button.attr('d', play)
        }
    }

    $('audio').on('timeupdate', function (err) {
        var proggress = Math.round(Splayer.currentTime / Splayer.duration * 100)
        var end = 100 - proggress;
        var end2 = proggress + 10;
        $("#player-song-progress").attr('value', proggress);
        // $("#player-buffered-progress").val(proggress);
        $("#range").val(proggress);
        var li_Status = $("#li-progress-" + currer_song)
        li_Status.css({'background': 'linear-gradient(to right, #7902a2 ' + proggress + '%, #ff660e ' + end2 + '%, #ff660e ' + end + '%)'})
        var current_time_min = Math.trunc(Splayer.currentTime / 60),
            current_time_sec = Math.trunc(Splayer.currentTime - (current_time_min * 60)),
            duration_time_min = Math.trunc(Splayer.duration / 60),
            duration_time_sec = Math.trunc(Splayer.duration - (duration_time_min * 60));
        $("#amplitude-current-minutes").text(current_time_min)
        current_time_sec = current_time_sec > 9 ? current_time_sec : '0' + current_time_sec;
        duration_time_sec = duration_time_sec > 9 ? duration_time_sec : '0' + duration_time_sec;
        $("#amplitude-current-seconds").text(current_time_sec);
        $("#amplitude-duration-minutes").text(duration_time_min);
        $("#amplitude-duration-seconds").text(duration_time_sec);

    });


    $("#progress-container").on('click', function (e) {
        var offset = $(this).offset(),
            left = e.clientX - offset.left,
            width = $(this).width();
        Splayer.currentTime = Splayer.duration * left / width;
    });
    return false;


    function updateAudion(id, url, name, artist) {
        last_song.attr('value', id)
        var status = $('#status-' + id)
        var bt = $("#song-" + id)
        var string = $("#string-" + id)
        bt.addClass(active)
        if (audio.attr('src') !== url) {
            audio.attr('src', url)
            status.attr('d', pause)
            play_pause.addClass('amplitude-playing')
            play_pause.removeClass('amplitude-paused')
            Splayer.play()
            string.attr('class', 'marquee')
            playing = play
        } else if (playing === play && audio.attr('src') === url) {
            Splayer.pause()
            playing = pause
            status.attr('d', play)
            play_pause.removeClass('amplitude-paused')
            play_pause.addClass('amplitude-playing')
            $("#audiojs_wrapper0").removeClass('playing')
            string.removeClass('marquee')
        } else {
            Splayer.play()
            status.attr('d', pause)
            play_pause.addClass('amplitude-playing')
            play_pause.addClass('amplitude-paused')
            $("#audiojs_wrapper0").addClass('playing')
            string.attr('class', 'marquee')
            playing = play
        }
        is_cached(url);
        cacheTitle(name, artist);
        cacheArtist(name, artist);
        song_name = name;
        song_artist = artist;
        currer_song = id;
    }


    function is_cached(src) {
        var db = openDatabase("MyDB", "0.1", "Пример базы данных WebSQL.", 200000);
        db.transaction((tx) => {
                tx.executeSql("CREATE TABLE uesrs (id REAL UNIQUE, login TEXT, first_name TEXT, last_name TEXT, email TEXT UNIQUE, phone TEXT, random INTEGER)", [])
            },
            err => console.error("Не могу создать таблицу users!", err),
            tx => console.log("Успешно создана таблица users")
        );
    }


    function updateAudionRepiat(id, url, name, artist) {
        last_song.attr('value', id)
        var status = $('#status-' + id)
        var bt = $("#song-" + id)
        var string = $("#string-" + id)
        bt.addClass(active)
        audio.attr('src', url)
        status.attr('d', pause)
        play_pause.addClass('amplitude-playing')
        play_pause.removeClass('amplitude-paused')
        Splayer.play()
        string.attr('class', 'marquee')
        playing = play
    }


    /*Изменения названия страницы*/
    function cacheTitle(name, artist) {
        document.title = name + ' - ' + artist;
    }

    /*Смена артиста*/
    function cacheArtist(name, artist) {
        $.ajax({
            url: "home/image/?name=" + name + " - " + artist,
            success: function (data) {
                $(".main-album-art").attr('src', data)
            }
        });
        player_song_name.text(artist);
        player_song_artist.text(name);
    }


});