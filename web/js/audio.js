class music_get{
    constructor(){
        this.el=document.getElementsByClassName("audio_row");
        this.s=[];
        this.j=[];
        this.last_len=this.el.length;
        this.make_str(this.el);
        this.load_audio_to_json(0,this.s[0]);
    }
    parse(element_){
        //функция, возвращающая id аудио, который надо передать в запросе, чтобы получить ссылку.
        //let i=JSON.parse(element_.attributes['data-audio'].nodeValue),s1=i[13].split("/");
        //return i[1]+"_"+i[0]+"_"+s1[2]+"_"+s1[s1.length-2];
        let i = AudioUtils.asObject(JSON.parse(element_.getAttribute('data-audio')));
        return i.fullId+"_"+i.actionHash+"_"+i.urlHash;
    }
    encode_url(t) {
        //функция, декодирующая ссылку на аудиозапись
        let c = {v:(t)=> {
            return t.split('').reverse().join('')
            },
            r: (t, e) => {t = t.split('');
            for (let i, o = _ + _, a = t.length; a--; )
                ~(i = o.indexOf(t[a])) && (t[a] = o.substr(i - e, 1));return t.join('')},
            s: (t,e)=> { let i = t.length;if (i) { let o = function(t, e) {let i = t.length,o = [];if (i) {let a = i;for (e = Math.abs(e); a--; ) e = (i * (a + 1) ^ e + a) % i,o[a] = e }return o}(t, e), a = 0;for (t = t.split(''); ++a < i; ) t[a] = t.splice(o[i - 1 - a], 1, t[a]) [0];t = t.join('')}return t},
            i:(t, e)=> {return c.s(t, e ^ vk.id)},x: (t, e)=> {let i = [];return e = e.charCodeAt(0),each(t.split(''), (t, o) => {i.push(String.fromCharCode(o.charCodeAt(0) ^ e))}),i.join('')}
        },_ = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN0PQRSTUVWXYZO123456789+/=',h=(t)=>{ if (!t || t.length % 4 == 1) return !1;for (var e, i, o = 0, a = 0, s = ''; i = t.charAt(a++); ) ~(i = _.indexOf(i)) && (e = o % 4 ? 64 * e + i : i, o++ % 4) && (s += String.fromCharCode(255 & e >> ( - 2 * o & 6)));return s};
        if ((!window.wbopen || !~(window.open + '').indexOf('wbopen')) && ~t.indexOf('audio_api_unavailable')) {
            let e = t.split('?extra=')[1].split('#'),i=''===e[1]?'':h(e[1]);
            if (e = h(e[0]), 'string' != typeof i || !e) return t;for (var o, a, s = (i = i ? i.split(String.fromCharCode(9))  : []).length; s--; ) {if (o = (a = i[s].split(String.fromCharCode(11))).splice(0, 1, e) [0], !c[o]) return t; e = c[o].apply(null, a)}if (e && 'http' === e.substr(0, 4)) return e}return t
    }
    end(){
        //для каждой аудиозаписи в html код добавляем кнопку
        each(this.j,(_,i)=>{
            let e = document.querySelectorAll('[data-full-id="'+i.fullId+'"]')[0],q=e.children[0].children[6];
            if(q.children.length===3)return;
            q.innerHTML+="<a href='"+this._g(i)+"' target='_blank' style='float:right;height:40px;width:40px;background:url(/doc472427950_504561254) no-repeat 5px 5px;'></a>"

        });
    }
    _g(info){
        if(info.url.indexOf(".mp3?")!==-1)
            return info.url;
        else
            return info.url.replace("/index.m3u8",".mp3").replace(/\/\w{11}\//,'/');
    }
    make_str(mass){
        //функция, добавляющая в массив s строки с id аудио, которые будут передаваться в запросе.
        each(mass,(i,e)=>{
            if(Math.floor(i/10)===i/10)
                this.s.push(this.parse(e));
            else
                this.s[this.s.length-1]+=","+this.parse(e);
        });
    }
    load_audio_to_json(i,l){
        //посылаем запрос на сервер вк, в котором в ответ приходит массив с аудио,
        //каждый элемент которого мы добавляем в массив this.j
        ajax.post("/al_audio.php",{act:'reload_audio',al:'1',ids:l},{onDone:(a)=>{
                //each - функция, которая есть на сайте vk.com - похожа на array.forEach
                each(a,(_,c)=>{
                    c=AudioUtils.asObject(c);
                    //ну естественно декодируем ссылку, как же без этого)
                    c.url = this.encode_url(c.url);
                    this.j.push(c);
                });
                //рекурсия
                if(this.s.length-1===i) this.end();
                else this.load_audio_to_json(i+1,this.s[i+1]);

            }});
    }
    _update_scroll(){
        //функция, вызывающаяся при скролле страницы.
        if(this.el.length===this.last_len)return;
        let c = this.el.length,offset=c-this.last_len;
        this.last_len=c;
        let arr = Array.from(this.el).splice(-offset);
        this._load_button(arr);
    }

    _load_button(list){
        //функция, которая подгружает новые кнопки.
        let leng=this.s.length-1;
        this.make_str(list);
        this.load_audio_to_json(leng,this.s[leng]);
    }
}


//функция скролла
var m = new music_get();
window.onscroll=()=>m._update_scroll();