<?php
header("Access-Control-Allow-Origin: *");
declare(ticks=1);

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../src/System/autoload.php';
require __DIR__.'/../src/System/config.php';
require __DIR__.'/../src/System/Routing.php';
require __DIR__.'/../src/System/TokenReceiverOfficial.php';
require __DIR__.'/../src/System/SupportedClients.php';
try {
    if(!isset($_SESSION)){
        ini_set('session.gc_maxlifetime', 31536000);
        session_start();
    }
    System\App::run();
} catch (ErrorException $e) {
    dump($e->getMessage());
}